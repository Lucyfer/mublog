package com.mublog

class Post {
    Date dateCreated
    String message
    
    static belongsTo = [user: ShiroUser]
    
    static constraints = {
        message size: 1..255, blank: false, nullable: false
    }
    
    static mapping = {
        version false
    }
    
    boolean subjectCanDelete(ShiroUser subject) {
        return this.user == subject || ShiroRole.findByName("admin").users.contains(subject)
    }
}
