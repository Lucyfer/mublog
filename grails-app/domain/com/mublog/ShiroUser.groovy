package com.mublog
import org.apache.shiro.crypto.hash.Sha256Hash

class ShiroUser {
    String username
    String passwordHash
    String confirm
    
    def isConfirmed() {
        if (this.passwordHash != this.confirm) {
            this.errors.rejectValue('confirm', 'user.password.doesnotmatch')
            return false
        }
        return true
    }
    
    def beforeInsert() {
        passwordHash = new Sha256Hash(passwordHash).toHex()
    }
    
    def beforeUpdate() {
        if (this.isDirty("passwordHash")) {
            passwordHash = new Sha256Hash(passwordHash).toHex()
        }
    }
    
    public String toString() {
        this.username
    }
    
    static transients = ['confirm']
    
    static hasMany = [roles: ShiroRole, permissions: String, posts: Post]

    static constraints = {
        username(nullable: false, blank: false, unique: true)
        passwordHash(nullable: false, blank: false)
    }

    static mapping = {
        version false
    }
}
