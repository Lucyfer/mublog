package com.mublog

class Subscribe {
    ShiroUser followee
    ShiroUser follower

    static constraints = {
        followee blank:false, nullable:false
        follower blank:false, nullable:false
    }
    
    static mapping = {
        version false
    }
}
