package com.mublog

class HomeController {

    def index = {
        def posts = Post.list(max:10, sort:"dateCreated", order:"desc")
        [posts:posts]
    }

    def search = {
        def query = params['q']
        def posts = query ? Post.findByMessageLike("%${query}%") : []
        [posts:posts, query:query]
    }
}
