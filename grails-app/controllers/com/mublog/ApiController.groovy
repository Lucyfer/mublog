package com.mublog

class ApiController {

    def index = { }
    
    def post = {
        def p = Post.get(params.id ? params.int('id') : -1)
        if (p) {
            render(contentType: "text/json") {
                [id: p.id, text: p.message, user_id: p.user.id, user: p.user.username, date: p.dateCreated.format("HH:mm, dd MMM yyyy")]
            }
        }
        else
            render(contentType: "text/json", status: 404) { error = "not found" }
    }

    def add = {
        def u = ShiroUser.findByUsernameAndPasswordHash(params.user, params.hash)
        if (u) {
            def p = new Post(message: params.text, user: u)
            if (p.save(flush: true)) {
                render(contentType: "text/json") { [id: p.id, text: p.message, user_id: p.user.id, user: p.user.username, date: p.dateCreated.format("HH:mm, dd MMM yyyy")] }
            }
            else
                render(contentType: "text/json") { error = message(code: "post.message.${p.errors.getFieldError("message").code}") }
        }
        else
            render(contentType: "text/json", status: 401) { error = "not authenticated" }
    }

    def delete = {
        def u = ShiroUser.findByUsernameAndPasswordHash(params.user, params.hash)
        if (u) {
            def p = Post.get(params.id ? params.int('id') : 0)
            if (p) {
                if (! p.subjectCanDelete(u))
                    return render(contentType: "text/json", status: 403) { error = "permission denied" }
                if (p.delete(flush: true)) 
                    render(contentType: "text/json") { status = "ok" }
                else
                    render(contentType: "text/json") { error = "can't delete" }
            }
            else
                render(contentType: "text/json", status: 404) { error = "not found" }
        }
        else
            render(contentType: "text/json", status: 401) { error = "not authenticated" }
    }

    def latest = {
        params.max = Math.min(params.max ? params.int('max') : 25, 25)
        params.offset = params.offset ? params.int('offset') : 0
        params.sort = 'dateCreated'
        params.order = 'desc'

        def ps = Post.withCriteria {
            if (params.after)
                gt('id', params.long('after'))
            firstResult(params.offset)
            maxResults(params.max)
            order(params.sort, params.order)
        }
        render(contentType: 'text/json') {
            array {
                for (p in ps) {
                    element id: p.id, text: p.message, user_id: p.user.id, user: p.user.username, date: p.dateCreated.format("HH:mm, dd MMM yyyy")
                }
            }
        }
    }

    def posted_by_user = {
        def u = ShiroUser.get(params.id ? params.int('id') : 0)
        if (u) {
            params.max = Math.min(params.max ? params.int('max') : 25, 25)
            params.offset = params.offset ? params.int('offset') : 0
            params.sort = 'dateCreated'
            params.order = 'desc'

            def ps = null
            if (params.after)
                ps = Post.findAllByUserAndIdGreaterThan(u, params.long('after'), params)
            else
                ps = Post.findAllByUser(u, params)

            render(contentType: "text/json") {
                array {
                    for (p in ps) {
                        element id: p.id, text: p.message, user_id: p.user.id, user: p.user.username, date: p.dateCreated.format("HH:mm, dd MMM yyyy")
                    }
                }
            }
        }
        else
            render(contentType: "text/json", status: 404) { error = "not found" }
    }
}
