package com.mublog
import org.apache.shiro.SecurityUtils

class SubscribeController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", on: "POST", off: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def on = {
        def followee = ShiroUser.get(params.id)
        def follower = ShiroUser.findByUsername(SecurityUtils.subject.principal)
        if (!followee)
            return render(view: 'error', model:[error: 'Пользователь не найден'])
        if (followee == follower)
            return render(view: 'error', model:[error: 'Нельзя подписаться на свой канал'])
        if (Subscribe.findByFolloweeAndFollower(followee, follower)) {
            return render(view: 'subscribed', model: [followee:followee])
        }
        new Subscribe(followee:followee, follower:follower).save()
        flash.message = message(code: 'subscribe.subscribe', args: [followee.username])
        if (params.targetUri)
            redirect(uri: params.targetUri)
        else
            redirect(controller: 'user', action: 'show', id: followee.id)
    }

    def off = {
        def followee = ShiroUser.get(params.id)
        def follower = ShiroUser.findByUsername(SecurityUtils.subject.principal)
        if (!followee)
            return render(view: 'error', model:[error: 'Пользователь не найден'])
        if (followee == follower)
            return render(view: 'error', model:[error: 'Нельзя подписаться на свой канал'])
        def s = Subscribe.findByFolloweeAndFollower(followee, follower)
        if (s) s.delete()
        flash.message = message(code: 'subscribe.unsubscribe', args: [followee.username])
        if (params.targetUri)
            redirect(uri: params.targetUri)
        else
            redirect(controller: 'user', action: 'show', id: followee.id)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [subscribeInstanceList: Subscribe.list(params), subscribeInstanceTotal: Subscribe.count()]
    }

    def create = {
        def subscribeInstance = new Subscribe()
        subscribeInstance.properties = params
        return [subscribeInstance: subscribeInstance]
    }

    def save = {
        def subscribeInstance = new Subscribe(params)
        if (subscribeInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'subscribe.label', default: 'Subscribe'), subscribeInstance.id])}"
            redirect(action: "show", id: subscribeInstance.id)
        }
        else {
            render(view: "create", model: [subscribeInstance: subscribeInstance])
        }
    }

    def show = {
        def subscribeInstance = Subscribe.get(params.id)
        if (!subscribeInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'subscribe.label', default: 'Subscribe'), params.id])}"
            redirect(action: "list")
        }
        else {
            [subscribeInstance: subscribeInstance]
        }
    }

    def edit = {
        def subscribeInstance = Subscribe.get(params.id)
        if (!subscribeInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'subscribe.label', default: 'Subscribe'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [subscribeInstance: subscribeInstance]
        }
    }

    def update = {
        def subscribeInstance = Subscribe.get(params.id)
        if (subscribeInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (subscribeInstance.version > version) {
                    
                    subscribeInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'subscribe.label', default: 'Subscribe')] as Object[], "Another user has updated this Subscribe while you were editing")
                    render(view: "edit", model: [subscribeInstance: subscribeInstance])
                    return
                }
            }
            subscribeInstance.properties = params
            if (!subscribeInstance.hasErrors() && subscribeInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'subscribe.label', default: 'Subscribe'), subscribeInstance.id])}"
                redirect(action: "show", id: subscribeInstance.id)
            }
            else {
                render(view: "edit", model: [subscribeInstance: subscribeInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'subscribe.label', default: 'Subscribe'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def subscribeInstance = Subscribe.get(params.id)
        if (subscribeInstance) {
            try {
                subscribeInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'subscribe.label', default: 'Subscribe'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'subscribe.label', default: 'Subscribe'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'subscribe.label', default: 'Subscribe'), params.id])}"
            redirect(action: "list")
        }
    }
}
