package com.mublog
import org.apache.shiro.SecurityUtils
import org.apache.shiro.authc.UsernamePasswordToken

class ShiroUserController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(controller: "user", action: "list", params: params)
    }
    
    def my = {
        def user = ShiroUser.findByUsername(SecurityUtils.subject.principal)
        def posts = Post.findAllByUser(user, [sort: 'dateCreated', order: 'desc'])
        [user: user, posts: posts]
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 50, 50)
        [users: ShiroUser.list(params), usersTotal: ShiroUser.count()]
    }

    def create = {
        def shiroUserInstance = flash.user ?: new ShiroUser(username: params.username)
        return [shiroUserInstance: shiroUserInstance]
    }

    def save = {
        def shiroUserInstance = new ShiroUser(username: params.username, passwordHash: params.password, confirm: params.confirm)
        shiroUserInstance.validate()
        if (shiroUserInstance.isConfirmed()) {
            shiroUserInstance.addToRoles(ShiroRole.findByName("user"))
            if (shiroUserInstance.save(flush: true)) {
                flash.message = message(code: 'default.created.message', args: [message(code: 'shiroUser.label', default: 'ShiroUser'), params.username])
                try {
                    SecurityUtils.subject.login(new UsernamePasswordToken(params.username, params.password))
                }
                catch (Exception e) {
                    // ignore
                }
                return redirect(controller: "user", action: "show", id: shiroUserInstance.id)
            }
        }
        //render(view: "create", model: [shiroUserInstance: shiroUserInstance])
        flash.user = shiroUserInstance
        redirect(controller: "user", action: "create")
    }

    def show = {
        def user = ShiroUser.get(params.id)
        if (!user) {
            //flash.message = message(code: 'default.not.found.message', args: [message(code: 'shiroUser.label', default: 'ShiroUser'), params.id])
            //redirect(controller: "user", action: "list")
            render(view: 'notfound')
        }
        else {
            def posts = Post.findAllByUser(user, [sort: 'dateCreated', order: 'desc'])
            [user: user, posts: posts]
        }
    }

    def edit = {
        def shiroUserInstance = flash.user ?: ShiroUser.get(params.id)
        if (!shiroUserInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'shiroUser.label', default: 'ShiroUser'), params.id])
            redirect(controller: "user", action: "list")
        }
        else {
            return [shiroUserInstance: shiroUserInstance]
        }
    }

    def update = {
        def shiroUserInstance = ShiroUser.get(params.id)
        if (shiroUserInstance) {
            shiroUserInstance.properties = params
            if (!shiroUserInstance.hasErrors() && shiroUserInstance.save(flush: true)) {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'shiroUser.label', default: 'ShiroUser'), shiroUserInstance.id])
                redirect(controller: "user", action: "show", id: shiroUserInstance.id)
            }
            else {
                flash.user = shiroUserInstance
                redirect(controller: "user", action: "edit")
            }
        }
        else {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'shiroUser.label', default: 'ShiroUser'), params.id])
            redirect(controller: "user", action: "list")
        }
    }

    def delete = {
        def shiroUserInstance = ShiroUser.get(params.id)
        if (shiroUserInstance) {
            try {
                shiroUserInstance.delete(flush: true)
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'shiroUser.label', default: 'ShiroUser'), params.id])
                redirect(controller:"user", action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'shiroUser.label', default: 'ShiroUser'), params.id])
                redirect(controller: "user", action: "show", id: params.id)
            }
        }
        else {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'shiroUser.label', default: 'ShiroUser'), params.id])
            redirect(controller: "user", action: "list")
        }
    }
}
