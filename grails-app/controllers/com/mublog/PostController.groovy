package com.mublog
import org.apache.shiro.SecurityUtils

class PostController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 25, 25)
        params.sort = 'dateCreated'
        params.order = 'desc'
        [postInstanceList: Post.list(params), postInstanceTotal: Post.count()]
    }

    def create = {
        def postInstance = new Post()
        if (flash.post)        
            postInstance = flash.post
        else
            postInstance.properties = params
        return [postInstance: postInstance]
    }

    def save = {
        def postInstance = new Post(params)
        postInstance.user = ShiroUser.findByUsername(SecurityUtils.subject.principal)
        if (postInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'post.label', default: 'Post'), postInstance.id])}"
            params.targetUri ? redirect(uri: params.targetUri) : redirect(action: "show", id: postInstance.id)
        }
        else {
            flash.post = postInstance
            redirect(controller: "post", action: "create")
        }
    }

    def show = {
        def postInstance = Post.get(params.id)
        if (!postInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'post.label', default: 'Post'), params.id])}"
            redirect(action: "list")
        }
        else {
            [postInstance: postInstance]
        }
    }

    def edit = {
        def postInstance = Post.get(params.id)
        if (!postInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'post.label', default: 'Post'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [postInstance: postInstance]
        }
    }

    def update = {
        def postInstance = Post.get(params.id)
        if (postInstance) {
            postInstance.properties = params
            if (!postInstance.hasErrors() && postInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'post.label', default: 'Post'), postInstance.id])}"
                params.targetUri ? redirect(uri: params.targetUri) : redirect(action: "show", id: postInstance.id)
            }
            else {
                render(view: "edit", model: [postInstance: postInstance])
            }
        }
        else {
            flash.error = "${message(code: 'default.not.found.message', args: [message(code: 'post.label', default: 'Post'), params.id])}"
            params.targetUri ? redirect(uri: params.targetUri) : redirect(action: "list")
        }
    }

    def delete = {
        def postInstance = Post.get(params.id)
        if (postInstance) {
            def user = ShiroUser.findByUsername(SecurityUtils.subject.principal)
            if ((postInstance.user != user) && !SecurityUtils.getSubject().hasRole("admin")) {
                flash.error = message(code: 'default.denied.message', args: ["Post #${params.id}"], default: "Access denied")
                return params.targetUri ? redirect(uri: params.targetUri) : redirect(controller: "post", action: "show", id: params.id)
            }
            try {
                postInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'post.label', default: 'Post'), params.id])}"
                params.targetUri ? redirect(uri: params.targetUri) : redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.error = "${message(code: 'default.not.deleted.message', args: [message(code: 'post.label', default: 'Post'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.error = "${message(code: 'default.not.found.message', args: [message(code: 'post.label', default: 'Post'), params.id])}"
            params.targetUri ? redirect(uri: params.targetUri) : redirect(action: "list")
        }
    }
}
