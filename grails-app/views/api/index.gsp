<g:applyLayout name="main">
    <head>
        <title>API documentation</title>
    </head>
<body>
    <div class="row">
        <div class="span12">
            <h1>API documentation</h1>

            <div class="well-large">
                <table class="table table-striped table-bordered">
                    <thead>
                        <th class="span5">Command</th><th>Reaction</th>
                    </thead>
                    <tr>
                        <td><pre>GET /api/post/:post_id</pre></td>
                        <td>
                            <p>Return post #post_id as Post object:<br/>
                            <code>{"id":12 (post_id), "text":"example", "user_id":4112 (user_id), "user":"owner's nickname", "date":"21:07, 29 feb 2012"}</code></p>
                            <p>If post is not found returns status 404 with object <code>{"error":"not found"}</code></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
<pre>
POST /api/delete/:post_id

Form parameters:
user = username
hash = sha256(password)
</pre>
                        </td>
                        <td>
                            <p>Delete post #post_id if authentification has been passed,
                            otherwise return HTTP Status 401 and object <code>{"error":"not authenticated"}</code></p>
                            <p>If post is not found returns status 404 with object <code>{"error":"not found"}</code></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
<pre>
POST /api/add

Form parameters:
user = username
hash = sha256(password)
text = your message
</pre>
                        </td>
                        <td>
                            Create new post if authentification has been passed,
                            otherwise return HTTP Status 403 and object <code>{"error":"not authenticated"}</code>
                        </td>
                    </tr>
                    <tr>
                        <td>
<pre>
GET /api/latest
GET /api/latest?max=10&offset=20&after=:post_id
</pre>
                        </td>
                        <td>
                            <p>
                                By default, get 25 latest posts. Optional you can set limit and/or offset.
                                Additional parameter `after` cuts off the posts older than #post_id.
                            </p>
                            <p>Format: <code>[Post object, Post object, ...]</code></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
<pre>
GET /api/posted_by_user/:user_id
GET /api/posted_by_user/:user_id?max=10&offset=20&after=:post_id
</pre>
                        </td>
                        <td>
                            <p>
                                Get 25 latest posts of the user with id = #user_id. Optional you can set limit and/or offset
                                Additional parameter `after` cuts off the posts older than #post_id.
                            </p>
                            <p>Format: <code>[Post object, Post object, ...]</code></p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</body>
</g:applyLayout>
