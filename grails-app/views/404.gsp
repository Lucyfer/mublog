<g:applyLayout name="main">
  <head>
    <title>404 Not Found</title>
  </head>
  
  <body>
      <h1>404 Not Found</h1>

      <div class="well well-large">
        Problem URI: <tt>${request.uri}</tt>
      </div>
  </body>
</g:applyLayout>