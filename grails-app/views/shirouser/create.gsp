<html>
    <head>
        <meta name="layout" content="main" />
        <title>Регистрация</title>
    </head>
<body>
  <h1>Регистрация</h1>
  <div class="well well-large">
      <g:hasErrors bean="${shiroUserInstance}">
      <div class="alert alert-error">
          <g:renderErrors bean="${shiroUserInstance}" as="list" />
      </div>
      </g:hasErrors>

      <g:form controller="user" action="save" method="post">
        <table>
          <tbody>
              <tr>
                  <td valign="top">
                      <label for="username">Логин</label>
                  </td>
                  <td valign="top" class="control-group ${hasErrors(bean: shiroUserInstance, field: 'username', 'error')}">
                      <g:textField name="username" value="${shiroUserInstance?.username}" />
                  </td>
              </tr>

              <tr>
                  <td valign="top">
                      <label for="password">Пароль</label>
                  </td>
                  <td valign="top" class="control-group ${hasErrors(bean: shiroUserInstance, field: 'passwordHash', 'error')}">
                      <g:passwordField name="password" value="${shiroUserInstance?.passwordHash}" />
                  </td>
              </tr>
              
              <tr>
                  <td valign="top">
                      <label for="confirm">Повторите пароль</label>
                  </td>
                  <td valign="top" class="control-group ${hasErrors(bean: shiroUserInstance, field: 'confirm', 'error')}">
                      <g:passwordField name="confirm" value="${shiroUserInstance?.confirm}" />
                  </td>
              </tr>
          </tbody>
        </table>
        <%--<g:submitButton name="create" class="btn-primary btn" value="Далее" />--%>
        <button type="submit" class="btn-primary btn">Далее</button>
        <button type="reset" class="btn-inverse btn">Сброс</button>
      </g:form>
  </div>
</body>
</html>
