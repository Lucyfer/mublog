<g:applyLayout name="main">
<html>
    <head>
        <title>Моя страница</title>
    </head>
<body>
    <h1>Моя страница</h1>
        <div class="row">
            <div class="span3">
                <%--<h3>Мои подписчики</h3>--%>
                <div class="well sidebar-nav">
                    <g:subscribers user="${user}" />
                </div>

                <%--<h3>Подписан на</h3>--%>
                <div class="well sidebar-nav">
                    <g:subscribeon user="${user}" />
                </div>
            </div>
            <div class="span9">
                <h2>Мои сообщения</h2>

                <g:render template="/post/show_post" var="post" collection="${posts}" />

                <g:unless test="${posts}">
                    <div class="well-large well">
                        <h3>Сообщений не найдено</h3>
                    </div>
                </g:unless>

                <g:render template="/post/form" />
            </div>
        </div>
</body>
</html>
</g:applyLayout>