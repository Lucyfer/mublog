<g:applyLayout name="main">
<html>
  <head>
    <title>Пользователи</title>
  </head>
  <body>
    <h1>Пользователи</h1>
    <div class="well-large">
        <div class="row">
            <div class="span6">
              <table class="table table-striped">
                  <g:each var="user" in="${users}" status="n">
                    ${ (n % 2 == 0) ? '<tr>' : '' }
                    <td><g:link controller="user" action="show" id="${user.id}">${user.username}</g:link></td>
                    ${ (n % 2 == 1) ? '</tr>' : '' }
                  </g:each>
              </table>

              <br />

              <div class="pagination">
                <g:paginate next="&rarr;" prev="&larr;" controller="user" action="list" total="${usersTotal}" />
              </div>
            </div>
        </div>
    </div>
  </body>
</html>
</g:applyLayout>