<g:applyLayout name="main">
<html>
  <head>
    <title>Страница пользователя</title>
  </head>
  <body>
      <h1>
          Страница ${user.username}
          <shiro:hasRole name="admin">
              [ <g:link controller="user" action="edit" id="${user.id}">Редактировать</g:link> ]
          </shiro:hasRole>
      </h1>

  <div class="info">
      <div class="row">
        <div class="span3">
          <%--<h3>Подписчики</h3>--%>
          <div class="well sidebar-nav">
            <g:subscribers user="${user}" />
            <g:subscribeSwitch followee="${user}" />
          </div>
          
          <%--<h3>Подписан на</h3>--%>
          <div class="well sidebar-nav">
            <g:subscribeon user="${user}" />
          </div>
        </div>

        <div class="span9">
          <h2>Сообщения пользователя</h2>

          <g:render template="/post/show_post" var="post" collection="${posts}" />

          <g:unless test="${posts}">
            <div class="well well-large">
              <h3>Сообщений не найдено</h3>
            </div>
          </g:unless>
        </div>

      </div>
    </div>
  </body>
</html>
</g:applyLayout>