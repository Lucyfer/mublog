<html>
    <head>
        <meta name="layout" content="main" />
        <title>Редактирование аккаунта</title>
    </head>
    <body>
        <h1>Редактирование аккаунта</h1>
        <div class="well-large">
            <g:hasErrors bean="${shiroUserInstance}">
                <div class="alert alert-error">
                    <g:renderErrors bean="${shiroUserInstance}" as="list" />
                </div>
            </g:hasErrors>

            <g:form method="post">
                <g:hiddenField name="id" value="${shiroUserInstance?.id}" />
                <div>
                    <table>
                        <tbody>
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="username"><g:message code="shiroUser.username.label" default="Username" /></label>
                                </td>
                                <td valign="top" class="control-group ${hasErrors(bean: shiroUserInstance, field: 'username', 'error')}">
                                    <g:textField name="username" value="${shiroUserInstance?.username}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="passwordHash"><g:message code="shiroUser.passwordHash.label" default="Password Hash" /></label>
                                </td>
                                <td valign="top" class="control-group ${hasErrors(bean: shiroUserInstance, field: 'passwordHash', 'error')}">
                                    <g:textField name="passwordHash" value="${shiroUserInstance?.passwordHash}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="permissions"><g:message code="shiroUser.permissions.label" default="Permissions" /></label>
                                </td>
                                <td valign="top" class="control-group ${hasErrors(bean: shiroUserInstance, field: 'permissions', 'error')}">
                                    <ul id="permissions">
                                        <g:each in="${shiroUserInstance?.permissions?}" var="p">
                                            <li>${p?.encodeAsHTML()}</li>
                                        </g:each>
                                    </ul>
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="posts"><g:message code="shiroUser.posts.label" default="Posts" /></label>
                                </td>
                                <td valign="top" class="control-group ${hasErrors(bean: shiroUserInstance, field: 'posts', 'error')}">
                                    <ul id="posts">
                                    <g:each in="${shiroUserInstance?.posts?}" var="p">
                                        <li><g:link controller="post" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
                                    </g:each>
                                    </ul>
                                    <g:link controller="post" action="create" params="['shiroUser.id': shiroUserInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'post.label', default: 'Post')])}</g:link>
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="roles"><g:message code="shiroUser.roles.label" default="Roles" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: shiroUserInstance, field: 'roles', 'errors')}">
                                    <g:select name="roles" from="${com.mublog.ShiroRole.list()}" multiple="yes" optionKey="id" size="5" value="${shiroUserInstance?.roles*.id}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div>
                    <g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    <g:actionSubmit class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </div>
            </g:form>
        </div>
    </body>
</html>
