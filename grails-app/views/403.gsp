<g:applyLayout name="main">
  <head>
    <title>403 Forbidden</title>
  </head>
  
  <body>
      <h1>403 Forbidden</h1>

      <div class="alert alert-error">
        You do not have permission to access this page.
      </div>
  </body>
</g:applyLayout>