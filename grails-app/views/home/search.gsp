<g:applyLayout name="main">
    <head></head>
    <body>
    <div class="row">
        <div class="span3">
            <div class="well sidebar-nav">
                <g:topUsers class="nav nav-list" />
            </div>
        </div>
        <div class="span9">
            <h2>Результаты поиска</h2>

            <g:if test="${query}">
                <h4>Для &laquo; ${query} &raquo;</h4>
                <g:render template="/post/show_post" var="post" collection="${posts}" />
                <g:unless test="${posts}">
                    <div class="hero-unit">
                        <h3>Совпадений не найдено</h3>
                    </div>
                </g:unless>
            </g:if>
            <g:else>
                <div class="hero-unit">
                    <h3>Не задан критерий для поиска</h3>
                </div>
            </g:else>
            <%--<g:render template="/post/form" />--%>
        </div>
    </div>
    </body>
</g:applyLayout>
