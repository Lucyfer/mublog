<g:applyLayout name="main">
  <head></head>
  <body>
    <div class="row">
      <div class="span3">
        <%--<h3>Топ пользователей</h3>--%>
        <div class="well sidebar-nav">
          <g:topUsers class="nav nav-list" />
        </div>
      </div>
      <div class="span9">
        <h2>Последние сообщения</h2>

        <g:render template="/post/show_post" var="post" collection="${posts}" />

        <g:unless test="${posts}">
          <div class="hero-unit">
            <h3>Сюда еще никто не успел <g:link controller="post" action="create">написать</g:link></h3>
          </div>
        </g:unless>

        <g:render template="/post/form" />
      </div>
    </div>
  </body>
</g:applyLayout>
