<html>
<head>
  <meta name="layout" content="main" />
  <title>Вход</title>
</head>
<body>
 <h2>Вход</h2>
 <div class="well well-large">
  <g:form action="signIn" method="post">
    <input type="hidden" name="targetUri" value="${targetUri}" />
    <table>
      <tbody>
        <tr>
          <td><label for="username">Логин</label></td>
          <td><input type="text" id="username" name="username" value="${username}" /></td>
        </tr>
        <tr>
          <td><label for="password">Пароль</label></td>
          <td><input type="password" id="password" name="password" value="" /></td>
        </tr>
        <tr>
          <td><label for="rmb">Запомнить?</label></td>
          <td><g:checkBox name="rememberMe" id="rmb" value="${rememberMe}" /></td>
        </tr>
        <tr>
          <td></td>
          <td><g:submitButton name="create" class="btn btn-large" value="Login" /></td>
        </tr>
      </tbody>
    </table>
  </g:form>
 </div>
</body>
</html>
