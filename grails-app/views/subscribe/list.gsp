<html>
    <head>
        <meta name="layout" content="main" />
        <title>Подписчики</title>
    </head>
    <body>

        <div class="info">
            <h1>Подписчики</h1>

            <div>
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'subscribe.id.label', default: 'Id')}" />
                        
                            <th><g:message code="subscribe.follower.label" default="Follower" /></th>
                        
                            <th><g:message code="subscribe.followee.label" default="Followee" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${subscribeInstanceList}" status="i" var="subscribeInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${subscribeInstance.id}">${fieldValue(bean: subscribeInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: subscribeInstance, field: "follower")}</td>
                        
                            <td>${fieldValue(bean: subscribeInstance, field: "followee")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${subscribeInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
