<html>
    <head>
        <meta name="layout" content="main" />
        <title>Подписка</title>
    </head>
    <body>

        <div class="info">
            <h1>Подписка</h1>

            <div class="info">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="subscribe.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: subscribeInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="subscribe.follower.label" default="Follower" /></td>
                            
                            <td valign="top" class="value"><g:link controller="shiroUser" action="show" id="${subscribeInstance?.follower?.id}">${subscribeInstance?.follower?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="subscribe.followee.label" default="Followee" /></td>
                            
                            <td valign="top" class="value"><g:link controller="shiroUser" action="show" id="${subscribeInstance?.followee?.id}">${subscribeInstance?.followee?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div>
                <g:form>
                    <g:hiddenField name="id" value="${subscribeInstance?.id}" />
                    <%-- <g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /> --%>
                    <g:actionSubmit class="button" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </g:form>
            </div>
        </div>
    </body>
</html>
