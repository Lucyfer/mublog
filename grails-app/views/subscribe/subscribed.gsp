<g:applyLayout name="main">
<html>
  <head>
    <title>Пользователи</title>
  </head>
  <body>
    <div class="info">
      <h1>Вы уже подписаны на ${followee.username}</h1>
      
      <g:link controller="user" action="show" id="${followee.id}">Перейти</g:link> 
      на его страницу или <g:link controller="subscribe" action="off" id="${followee.id}">отписаться</g:link>.
    </div>
  </body>
</html>
</g:applyLayout>