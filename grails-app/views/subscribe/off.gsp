<g:applyLayout name="main">
<html>
  <head>
    <title>Пользователи</title>
  </head>
  <body>
      <h1>Вы отписались от ${followee.username}</h1>
    <div class="well-large">
      <g:link controller="user" action="show" id="${followee.id}">Перейти</g:link> на его страницу.
    </div>
  </body>
</html>
</g:applyLayout>