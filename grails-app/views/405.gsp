<g:applyLayout name="main">
    <head>
        <title>405 Method not allowed</title>
    </head>

    <body>
        <h1>405 Method not allowed</h1>

        <div class="well well-large">
            Resource not available.
        </div>
    </body>
</g:applyLayout>