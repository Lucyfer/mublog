<!doctype html>
<html lang="ru">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <title><g:layoutTitle default="μBlog" /></title>
  <link rel="stylesheet" href="${resource(dir:'css',file:'bootstrap.css')}" />
  <link rel="stylesheet" href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />
  <link rel="shortcut icon" href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-icon" />
  <style type="text/css">
    body {
        padding-top: 60px;
    }
  </style>
  <g:javascript library="jquery-1.7.1.min" />
  <g:javascript library="bootstrap" />
  <g:javascript library="bootstrap-alert" />
  <g:javascript library="application" />
  <g:layoutHead />
  <!--[if lt IE 9]><script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>

<body>

    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container-fluid">
                <!--a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a-->
                <a class="brand" href="/">μBlog</a>
                <p class="navbar-text pull-left">Сервис микроблоггов</p>
                <!--div class="navbar-text pull-left" style='border-right:#555555 1px dotted'>&nbsp;</div-->
                <div class="nav-collapse">
                    <ul class="nav">
                        <li class="divider-vertical"></li>
                        <li><a href="/"><i class="icon-home icon-white"></i> Home</a></li>
                        <li><g:link controller="post" action="create"><i class="icon-pencil icon-white"></i> New Post</g:link></li>
                        <li><g:link controller="user" action="my"><i class="icon-user icon-white"></i> My Page</g:link></li>
                        <li class="divider-vertical"></li>
                    </ul>
                </div>
                <div class="nav-collapse pull-left">
                    <g:form controller="home" action="search" class="navbar-search" method="get">
                        <input name="q" type="text" class="span2 search-query small" placeholder="Search" />
                    </g:form>
                </div>
                <ul class="nav">
                    <li class="divider-vertical"></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Docs<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><g:link controller="api" action="index"><i class="icon-book"></i> API</g:link></li>
                        </ul>
                    </li>
                </ul>
                <div class="nav-collapse pull-right">
                    <shiro:user>
                        <div class="navbar-text pull-left">
                            Logged in as <g:link controller="user" action="my"><shiro:principal /></g:link>
                        </div>
                        <ul class="nav">
                            <li class="divider-vertical"></li>
                            <li><g:link controller="auth" action="signOut">Log Out</g:link></li>
                        </ul>
                    </shiro:user>
                    <shiro:notUser>
                        <div class="pull-right">
                            <ul class="nav">
                                <li><g:link controller="auth" action="login">Log In</g:link></li>
                                <li><g:link controller="user" action="create">Registration</g:link></li>
                            </ul>
                        </div>
                    </shiro:notUser>
                </div>
            </div>
        </div>
    </div>

<div class="container">

    <g:if test="${flash.message}">
        <div class="alert alert-info">
            <a class="close" data-dismiss="alert">×</a>
            ${flash.message}
        </div>
    </g:if>

    <g:if test="${flash.error}">
        <div class="alert alert-error">
            <a class="close" data-dismiss="alert">×</a>
            <h4 class="alert-heading">Error!</h4>
            ${flash.error}
        </div>
    </g:if>

    <g:layoutBody />

    <hr>

    <footer>
        <p>&copy; Simple microblog 2012</p>
    </footer>
</div>

</body>
</html>
