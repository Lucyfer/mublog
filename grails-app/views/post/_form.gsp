<g:hasErrors bean="${post}">
    <div class="alert alert-error">
        <g:renderErrors bean="${post}" as="list" />
    </div>
</g:hasErrors>

<g:form controller="post" action="save" method="post">
    <div class="control-group ${hasErrors(bean: post, field: 'message', 'error')}">
        <g:textArea class="input-xxlarge" name="message" value="${post?.message}" rows="2" cols="65" />
    </div>
    <g:submitButton name="create" class="btn" value="Написать" />
</g:form>