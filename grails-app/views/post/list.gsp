<html>
<head>
  <meta name="layout" content="main" />
  <title>Последние сообщения</title>
</head>
<body>
    <h1>Последние сообщения</h1>
    <div class="well-large">
        <g:if test="${postInstanceList}">
            <g:render template="/post/show_post" var="post" collection="${postInstanceList}" />
            <div class="pagination">
                <g:paginate next="&rarr;" prev="&larr;" controller="user" action="list" total="${postInstanceTotal}" />
            </div>
        </g:if>
        <g:else>
            <div class="hero-unit">
                <h3>Сюда еще никто не успел <g:link controller="post" action="create">написать</g:link></h3>
            </div>
        </g:else>
  </div>
</body>
</html>
