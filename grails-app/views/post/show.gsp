<g:applyLayout name="main">
<html>
    <head>
        <title>Показать отдельный пост</title>
    </head>
    <body>
        <h1>Пост #${postInstance.id}</h1>
        <div class="well-large">
            <g:render template="/post/show_post" var="post" bean="${postInstance}" />
        </div>
    </body>
</html>
</g:applyLayout>