<div class="row">
    <div class="span9">
        <a name="post${post.id}"></a>
        <div class="well well-small">
            <%--<span class="silver">--%>
            <strong><a href="${createLink(controller:"user", action:"show", id:post.user.id)}">${post.user.username}</a></strong>
            <%--</span>--%>
            &nbsp;
            <g:link class="label" controller="post" action="show" id="${post.id}">
                #${post.id} ${post.dateCreated.format('HH:mm, dd MMM yyyy')}
            </g:link>
            <shiro:hasRole name="admin">
              &nbsp;
              <g:link class="btn-mini btn btn-info" controller="post" action="edit" id="${post.id}">Edit</g:link>
              <g:form style="display: inline;" class="form-inline" controller="post" action="delete" id="${post.id}" method="post">
                  <g:hiddenField name="targetUri" value="${request.forwardURI}" />
                  <g:submitButton class="btn btn-mini btn-danger" name="delete" value="Delete" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}')" />
              </g:form>
            </shiro:hasRole>

            <shiro:hasRole name="user">
              <g:canDeletePost id="${post.id}">
                &nbsp;
                <g:form style="display: inline;" class="form-inline" controller="post" action="delete" id="${post.id}" method="post">
                    <g:hiddenField name="targetUri" value="${request.forwardURI}" />
                    <g:submitButton class="btn-mini btn btn-danger" name="delete" value="Delete" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}')" />
                </g:form>
              </g:canDeletePost>
            </shiro:hasRole>

            <%--<div class="silver_border">--%>
            <blockquote>
                <p> ${post.message} </p>
            </blockquote>
            <%--</div>--%>
        </div>
    </div>
</div>
