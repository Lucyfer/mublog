<html>
    <head>
        <meta name="layout" content="main" />
        <title>Редактирование сообщения</title>
    </head>
    <body>
        <h1>Редактирование сообщения</h1>
        <div class="well-large">
            <g:hasErrors bean="${postInstance}">
                <div class="alert alert-error">
                    <g:renderErrors bean="${postInstance}" as="list" />
                </div>
            </g:hasErrors>

            <g:form method="post">
                <g:hiddenField name="id" value="${postInstance?.id}" />
                <g:hiddenField name="version" value="${postInstance?.version}" />
                <div class="control-group ${hasErrors(bean: postInstance, field: 'message', 'error')}">
                    <g:textArea class="input-xxlarge" name="message" cols="65" rows="2" value="${postInstance?.message}" />
                </div>
                <div>
                    <g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    <g:actionSubmit class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </div>
            </g:form>
        </div>
    </body>
</html>
