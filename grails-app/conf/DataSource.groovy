/*
dataSource {
    pooled = true
    driverClassName = "org.hsqldb.jdbcDriver"
    username = "sa"
    password = ""
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = true
    cache.provider_class = 'net.sf.ehcache.hibernate.EhCacheProvider'
}
// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "update" // one of 'create', 'create-drop','update'
            url = "jdbc:hsqldb:mem:devDB"
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:hsqldb:mem:testDb"
        }
    }
    production {
        dataSource {
            dbCreate = "update"
            url = "jdbc:hsqldb:file:prodDb;shutdown=true"
        }
    }
}
*/
dataSource {
    pooled = true
    driverClassName = "org.postgresql.Driver"
    dialect = org.hibernate.dialect.PostgreSQLDialect
}
hibernate {
    cache.use_second_level_cache=true
    cache.use_query_cache=true
    //cache.provider_class='com.opensymphony.oscache.hibernate.OSCacheProvider'
    //cache.provider_class='net.sf.ehcache.hibernate.SingletonEhCacheRegionFactory'
    cache.provider_class = 'net.sf.ehcache.hibernate.EhCacheProvider'
    //hibernate.flush.mode="manual"
}
// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "update"
            url = "jdbc:postgresql://localhost:5432/nirs_db"
            username = "nirs_user"
            password = "nirs_user+"
        }   
    }   
    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:postgresql://localhost:5432/nirs_db"
            username = "nirs_user"
            password = "nirs_user+"
        }   
    }   
    production {
        dataSource {
            dbCreate = "update"
            url = "jdbc:postgresql://localhost:5432/nirs_db"
            username = "nirs_user"
            password = "nirs_user+"
        }   
    }   
} 