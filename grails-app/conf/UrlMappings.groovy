class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}
        
        "/user/$action?/$id?" {
            controller = "shiroUser"
        }

		//"/"(controller:'home', action:'index')
        "/$action?"(controller:'home')

		"500"(view:'/error')
        "405"(view:'/405')
        "404"(view:'/404')
        "403"(view:'/403')
	}
}
