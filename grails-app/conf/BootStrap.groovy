import com.mublog.*

class BootStrap {
    def init = { servletContext ->
        def admin_role = ShiroRole.findByName("admin") ?: new ShiroRole(name: "admin").save(failOnError: true, flush: true)
        def user_role =  ShiroRole.findByName("user") ?: new ShiroRole(name: "user").save(failOnError: true, flush: true)
        def user1 = ShiroUser.findByUsername("user1") ?: new ShiroUser(username: "user1", passwordHash: "password").save(failOnError: true, flush: true)
        def user2 = ShiroUser.findByUsername("user2") ?: new ShiroUser(username: "user2", passwordHash: "password").save(failOnError: true, flush: true)

        if (! admin_role.users?.contains(user1)) {
            admin_role.addToUsers(user1)
        }        
        if (! user_role.users?.contains(user2)) {
            user_role.addToUsers(user2)
        }
        if (! admin_role.permissions?.count()) {
            admin_role.addToPermissions("*:*").save(failOnError: true)
        }
        if (! user_role.permissions?.count()) {
            user_role
                    .addToPermissions("shiroUser:my")
                    .addToPermissions("post:create,save,delete")
                    .addToPermissions("subscribe:on,off")
                    .save(failOnError: true)
        }
    }
    def destroy = {
    }
}
