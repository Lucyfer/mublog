package com.mublog

class TopusersTagLib {
    def topUsers = { attrs ->
        StringBuilder sb = new StringBuilder()
        //select b,count(b) as c from subscribe group by b order by c desc
        def result = Subscribe.withCriteria {
            projections {
                groupProperty('followee')
                count('followee', 'fCount')
            }
            order('fCount', 'desc')
            maxResults(10)
        }
        if (result) {
            def css = attrs.collect{ k,v -> "${k}=\"${v}\"" }.join(' ')
            sb << "<ul ${css}>"
            sb << "<li class='nav-header'>Топ пользователей</li>"
            for (res in result) {
                def user = res[0]
                def flws = res[1]
                sb << "<li><a href='${createLink(controller:'user', action:'show', id:user.id)}'>${user.username} (${flws})</a></li>"
            }
            sb << "</ul>"
        }
        out << sb.toString()
    }
}
