package com.mublog

class PermissionsTagLib {
    def canDeletePost = { attrs, body ->
        def user = ShiroUser.findByUsername(shiro.principal())
        def post = Post.get(attrs.id)
        if (user == post.user) {
            out << body()
        }
    }
}
