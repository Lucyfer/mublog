package com.mublog

class SubscribeTagLib {
    def subscribers = { attrs ->
        StringBuilder sb = new StringBuilder()
        def subs = Subscribe.findAllByFollowee(attrs.user)
        sb << "<ul class='nav nav-list'>"
        sb << "<li class='nav-header'>Подписчики</li>"
        if (subs) {
            for(res in subs) {
                def user = res.follower
                sb << "<li><a href='${createLink(controller:'user', action:'show', id:user.id)}'>${user.username}</a></li>"
            }
        }
        else
            sb << "<li><small>Нет подписчиков</small></li>"
        sb << "</ul>"
        out << sb.toString()
    }
    
    def subscribeon = { attrs ->
        StringBuilder sb = new StringBuilder()
        def subs = Subscribe.findAllByFollower(attrs.user)
        sb << "<ul class='nav nav-list'>"
        sb << "<li class='nav-header'>Подписан на</li>"
        if (subs) {
            for(res in subs) {
                def user = res.followee
                sb << "<li><a href='${createLink(controller:'user', action:'show', id:user.id)}'>${user.username}</a></li>"
            }
        }
        else
            sb << "<li><small>Нет подписок</small></li>"
        sb << "</ul>"
        out << sb.toString()
    }

    def subscribeSwitch = { attrs ->
        StringBuilder sb = new StringBuilder()
        def follower = ShiroUser.findByUsername(shiro.principal())
        if (attrs.followee.id != follower?.id) {
            def unsub = Subscribe.findByFolloweeAndFollower(attrs.followee, follower)
            sb << '<p>'
            sb << "<form id='subscribe' action='${createLink controller:'subscribe', action: unsub ? 'off' : 'on', id:attrs.followee.id}' method='post'>"
            sb << hiddenField(name: 'targetUri', value: request.forwardURI)
            sb << submitButton(name: 'submit', value: unsub ? 'Отписаться' : 'Подписаться', class: unsub ? 'btn btn-inverse' : 'btn btn-primary')
            sb << "</form>"
            sb << '</p>'
        }
        out << sb.toString()
    }
}
